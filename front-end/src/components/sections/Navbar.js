import React from "react";
import { Navbar, Nav, Button } from "react-bootstrap";
export const CustomNavbar = (props) => {
  const LogOut = () => {
    console.log("Logging out...");
  };
  return (
    <Navbar expand="md">
      <Navbar.Brand href="/">Survapp</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link
            href="#home"
            onClick={() => console.log('"Home" selected!')}
          >
            Home
          </Nav.Link>
          <Nav.Link
            href="#account"
            onClick={() => console.log('"Surveys" selected!')}
          >
            Surveys
          </Nav.Link>
          <Nav.Link
            href="#account"
            onClick={() => console.log('"My Account" selected!')}
          >
            Account
          </Nav.Link>
        </Nav>
        <Nav>
          <Nav.Link href="#logout" onClick={LogOut} variant="primary">
            Login
          </Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
};
