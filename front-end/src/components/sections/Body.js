import React from "react";
import { CustomSidebar } from "./body/Sidebar";
import { Main } from "./body/Main";
import { Row, Col } from "react-bootstrap";

export const Body = () => {
  return (
    <Row>
      <Col md="3">
        <CustomSidebar />
      </Col>
      <Col md="9">
        <Main />
      </Col>
    </Row>
  );
};
