import React from "react";
import SurveyForm from "../SurveyForm";

export const SelectedSurvey = (props) => (
  <SurveyForm survey={props.survey} setSurvey={props.setSurvey} />
);
