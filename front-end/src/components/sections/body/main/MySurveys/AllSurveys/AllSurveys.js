import React from "react";
import { Table } from "react-bootstrap";

export const TableSurveys = (props) => (
  <>
    <h2>All Surveys</h2>
    <span className="font-weight-light">Select a survey to edit.</span>
    <Table striped>
      <thead>
        <tr>
          <th>ID</th>
          <th>Name</th>
          <th>Description</th>
        </tr>
      </thead>
      <tbody>
        {props.surveys.map((s) => (
          <SurveyRow key={s.id} survey={s} selectSurvey={props.selectSurvey} />
        ))}
      </tbody>
    </Table>
  </>
);

const SurveyRow = (props) => {
  const survey = props.survey;
  return (
    <tr onClick={() => props.selectSurvey(survey)}>
      <td>{survey.id}</td>
      <td>{survey.name}</td>
      <td>{survey.description}</td>
    </tr>
  );
};
