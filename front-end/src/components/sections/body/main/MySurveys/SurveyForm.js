import React, { useEffect, useState } from "react";
import { Row, Col, Form } from "react-bootstrap";
import { graphQLApi } from "../../../../../utils";
import { updateSurvey } from "../../../../../mutations/SurveyMutations";

//props: survey
export default (props) => {
  const survey = props.survey;
  const [categories, setCategories] = useState([]);

  const answerQuestion = (entryId, targetInputEvent) => {
    const copy = { ...survey };
    copy.surveyEntries.forEach((se) => {
      if (se.id === entryId) {
        se.answer = targetInputEvent.target.value;
      }
    });

    props.setSurvey(copy);
  };

  useEffect(() => {
    let _categories = {};
    survey.surveyEntries.forEach((se) => {
      const c = se.question.category;
      _categories[c.id] = c;
    });
    setCategories([...Object.values(_categories)]);
  }, [survey]);

  const save = () => {
    graphQLApi.post("", updateSurvey(survey));
  };

  return (
    <>
      <h1 className="border-bottom">Checklist</h1>
      {categories
        .sort((x, y) => x.sortId < y.sortId)
        .map((c) => {
          //console.log(`category ${c.name} with sortId ${c.sortId}`);
          return (
            <Row key={c.id}>
              <Col>
                <Row>
                  <Col>
                    <h3>{c.name}</h3>
                  </Col>
                </Row>
                <Row>
                  {survey.surveyEntries.map((se) => {
                    if (se.question.category.name === c.name) {
                      //console.log(
                      //`${se.question.questionText} ${se.question.sortId}`
                      // );
                      return (
                        <SurveyQuestion
                          key={se.id}
                          answerQuestion={answerQuestion}
                          surveyEntry={se}
                          save={save}
                        />
                      );
                    }
                  })}
                </Row>
              </Col>
            </Row>
          );
        })}
    </>
  );
};

const SurveyQuestion = (props) => {
  const surveyEntry = props.surveyEntry;
  return (
    <Col md={3}>
      <Form.Group controlId={"controlId-" + surveyEntry.id}>
        <Form.Label>{surveyEntry.question.questionText}</Form.Label>
        <Form.Control
          type="text"
          value={surveyEntry.answer}
          onBlur={props.save}
          onChange={(e) => {
            props.answerQuestion(surveyEntry.id, e);
          }}
        />
      </Form.Group>
    </Col>
  );
};
