import React, { useState, useEffect } from "react";
import { Button, Row, Col, Form } from "react-bootstrap";
import { graphQLApi } from "../../../../../../utils";
import { allQuestionCategories } from "../../../../../../queries/questionQueries";

export const NewSurvey = () => {
  const [allCategories, setAllCategories] = useState([]);
  const [categories, setCategories] = useState([]);
  const [questions, setQuestions] = useState([]);

  const setQuestion = (questionId, e) => {};

  const save = () => {};

  const addCategory = () => {};

  useEffect(() => {
    graphQLApi.post("", allQuestionCategories()).then((resp) => {
      setAllCategories(resp.data.data.allQuestionCategories);
    });
  }, []);

  return (
    <>
      <Button onClick={addCategory} variant="primary">
        Add Category
      </Button>
      {categories.map((c) => (
        <CategorySection category={c} questions={questions} />
      ))}
    </>
  );
};

const CategorySection = (props) => {
  const category = props.category;
  const questions = props.questions;
  return (
    <>
      <Row className="justify-content-between border">
        <Col sm={10}>
          <h2>
            {category.name}
            <Button size="sm" className="ml-3" variant="danger">
              -
            </Button>
          </h2>
        </Col>
        <Col sm={2} className="text-right">
          <Button size="sm" variant="success">
            +
          </Button>
          <Button size="sm" variant="danger">
            -
          </Button>
        </Col>
      </Row>
      <Row>
        {questions
          .filter((q) => q.category.id === c.id)
          .map((q) => {
            <Col>
              <Form.Group controlId={"controlId-" + q.id}>
                <Form.Control
                  type="text"
                  value={q.questionText}
                  onBlur={props.save}
                  onChange={(e) => {
                    props.setQuestion(q.id, e);
                  }}
                />
              </Form.Group>
            </Col>;
          })}
      </Row>
    </>
  );
};
