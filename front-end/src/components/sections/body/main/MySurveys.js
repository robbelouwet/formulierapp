import React, { useEffect, useState } from "react";
import { Nav } from "react-bootstrap";
import { graphQLApi } from "../../../../utils";
import { metaAllSurveys } from "../../../../queries/SurveyQueries";
import { Link, Route } from "react-router-dom";
import { TableSurveys } from "./MySurveys/AllSurveys/AllSurveys";
import { NewSurvey } from "./MySurveys/NewSurvey/NewSurvey";
import { SelectedSurvey } from "./MySurveys/SelectedSurvey/SelectedSurvey";
import { surveyWithQuestions } from "../../../../queries/SurveyQueries";
export const MySurveys = () => {
  const [surveys, setSurveys] = useState([]);
  const [selectedSurvey, setSelectedSurvey] = useState({});
  const [isSelected, setSelected] = useState(false);

  useEffect(() => {
    graphQLApi
      .post("", metaAllSurveys())
      .then((resp) => setSurveys(resp.data.data.allSurveys));
  }, []);

  const selectSurvey = (survey) => {
    if (selectedSurvey.id === survey.id) {
      deselectSurvey();
      return;
    }
    graphQLApi
      .post("", surveyWithQuestions(survey.id))
      .then((resp) => setSelectedSurvey(resp.data.data.surveyWithQuestions));
    setSelectedSurvey(survey);
    setSelected(true);
  };

  const deselectSurvey = () => {
    setSelectedSurvey({});
    setSelected(false);
  };

  return (
    <>
      <SurveyNav isSelected={isSelected} survey={selectedSurvey} />
      <Route
        path="/mysurveys/"
        exact
        render={() => (
          <TableSurveys selectSurvey={selectSurvey} surveys={surveys} />
        )}
      />
      <Route
        path="/mysurveys/allsurveys"
        render={() => (
          <TableSurveys selectSurvey={selectSurvey} surveys={surveys} />
        )}
      />
      <Route
        path="/mysurveys/selectedsurvey"
        render={() => (
          <SelectedSurvey
            survey={selectedSurvey}
            setSurvey={setSelectedSurvey}
          />
        )}
      />
      <Route path="/mysurveys/newsurvey" component={NewSurvey} />
    </>
  );
};

const SurveyNav = (props) => {
  const [activeTab, setActiveTab] = useState(1);

  const e = typeof props.survey.name == "undefined";
  const tabTitle = e ? "Selected Survey" : props.survey.name;
  return (
    <Nav variant="tabs" defaultActiveKey="/home">
      <Nav.Item>
        <Nav.Link active={activeTab === 1}>
          <Link to="/mysurveys/allsurveys" onClick={() => setActiveTab(1)}>
            All Surveys
          </Link>
        </Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link
          disabled={props.isSelected ? "" : "disabled"}
          active={activeTab === 2}
        >
          <Link to="/mysurveys/selectedsurvey" onClick={() => setActiveTab(2)}>
            {tabTitle}
          </Link>
        </Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link active={activeTab === 3}>
          <Link to="/mysurveys/newsurvey" onClick={() => setActiveTab(3)}>
            New Survey
          </Link>
        </Nav.Link>
      </Nav.Item>
    </Nav>
  );
};
