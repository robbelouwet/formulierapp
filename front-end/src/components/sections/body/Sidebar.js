import React from "react";
import { ListGroup, ListGroupItem } from "react-bootstrap";
import { NavLink, Link } from "react-router-dom";

export const CustomSidebar = () => {
  return (
    <ListGroup>
      <Link to="/mysurveys">
        <ListGroupItem className="text-center">My Surveys</ListGroupItem>
      </Link>

      <Link to="/questions">
        <ListGroupItem className="text-center">Questions</ListGroupItem>
      </Link>

      <Link to="respondents">
        <ListGroupItem className="text-center">Respondents</ListGroupItem>
      </Link>
    </ListGroup>
  );
};
