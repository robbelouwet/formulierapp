import React from "react";
import { MySurveys } from "./main/MySurveys";
import { Questions } from "./main/Questions";
import { Respondents } from "./main/Respondents";
import { Route } from "react-router-dom";

export const Main = () => {
  return (
    <>
      <Route path="/mysurveys" component={MySurveys} />
      <Route path="/questions" component={Questions} />
      <Route path="/respondents" component={Respondents} />
    </>
  );
};
