import React from "react";
import { CustomNavbar } from "./sections/Navbar";
import { Body } from "./sections/Body";
import { Container, Row, Col } from "react-bootstrap";

export const Page = () => {
  return (
    <Container fluid className="p-0">
      <Row>
        <Col>
          <CustomNavbar />
        </Col>
      </Row>
      <Body />
    </Container>
  );
};
