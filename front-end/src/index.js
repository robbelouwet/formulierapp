import "./styles/style.scss";
import React from "react";
import ReactDOM from "react-dom";
import { Page } from "./components/Page";
import { BrowserRouter, HashRouter } from "react-router-dom";

ReactDOM.hydrate(
  <BrowserRouter>
    <Page />
  </BrowserRouter>,
  document.getElementById("mountNode")
);
