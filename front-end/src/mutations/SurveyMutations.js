export const updateSurvey = (survey) => {
  return {
    query: `mutation UpdateSurvey($survey: SurveyInputType!) {
                updateSurvey(survey: $survey) {
                    id
                }
          }  
        `,
    variables: {
      survey: survey,
    },
  };
};
