export const allQuestionCategories = () => {
  return {
    query: `{
        allQuestionCategories {
            id,
            description,
            sortId,
            name
        }
    }`,
  };
};
