export const metaAllSurveys = () => {
  return {
    query: `{
        allSurveys {
            id,
            name,
            description
        }
    }`,
  };
};

export const surveyWithQuestions = (id) => {
  return {
    query: `{
            surveyWithQuestions(surveyId: "${id}") {
              id,
              name,
              description,
              stakeholders {
                userName,
                description,
                email
              },
              surveyEntries {
                id,
                answer,
                question {
                  id,
                  questionText,
                  sortId,
                  category {
                    id,
                    name,
                    description,
                    sortId
                  }
                }
              }
            }
        }`,
  };
};
