import axios from "axios";

export const api = axios.create({
  baseURL: "https://localhost:5001/api/",
  headers: {
    "Access-Control-Allow-Origin": "*", //enable cors to ASP.NET Core server
  },
});

export const graphQLApi = axios.create({
  baseURL: "https://localhost:5001/graphql/",
  headers: {
    "Access-Control-Allow-Origin": "*",
  },
});
