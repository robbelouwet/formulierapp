import express from "express";
import cors from "cors";
import React from "react";
import ReactDOMServer from "react-dom/server";
import { Page } from "../components/Page";
import { StaticRouter } from "react-router-dom";

const server = express();
server.use(cors());
server.use(express.static("dist"));

server.get("/", (req, res) => {
  const context = {};
  const initialMarkup = ReactDOMServer.renderToString(
    <StaticRouter location={req.url} context={context}>
      <Page />
    </StaticRouter>
  );
  res.send(`
    <html>
      <head>
        <title>Sample React App</title>
        <link rel="stylesheet" href="/index.css">
      </head>
      <body>
        <div id="mountNode">${initialMarkup}</div>
        <script src="/bundle.js"></script>
      </body>
    </html>`);
});

server.listen(4242, () => console.log("Server is running..."));
