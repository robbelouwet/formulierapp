﻿using System;
using Domain.Survey;
using Domain.Survey.Question;

namespace BL.Service
{
    public interface IQuestionService
    {
        Question GetQuestion(Guid questionId);
        bool CreateQuestion(string questionText, int sortId, QuestionCategory category);
        bool ChangeQuestion(Guid questionId, string questionText, int sortId);
        bool DeleteQuestion(Guid questionId);
    }
}