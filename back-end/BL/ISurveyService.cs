﻿using System;
using System.Collections;
using System.Collections.Generic;
using Domain.Survey;
using Domain.Survey.Question;

namespace BL
{
    public interface ISurveyService
    {
        Survey GetSurvey(Guid listId);
        IEnumerable<Survey> GetAllSurveys();
        Survey GetSurveyQuestionsAndAnswers(Guid listId);
        void RemoveSurvey(Guid listId);
        // Guid CreateSurveyTemplate();
        void AnswerSurveyQuestion(Guid surveyId, Guid QuestionId, string answerText);
    }
}