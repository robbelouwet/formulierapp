﻿using System;
using DAL;
using DAL.Repository.Generic;
using Domain.Survey;
using Domain.Survey.Question;

namespace BL.Service
{
    public class QuestionService : IQuestionService
    {
        private readonly IQuestionRepository _questionRepo;

        public QuestionService(IQuestionRepository questionRepo)
        {
            _questionRepo = questionRepo;
        }
        
        public Question GetQuestion(Guid questionId)
        {
            return _questionRepo.ReadById(questionId);
        }

        public bool CreateQuestion(string questionText, int sortId, QuestionCategory category)
        {
            Question q = new Question()
            {
                QuestionText = questionText,
                SortId = sortId,
                Category = category
            };
            //Not sure if this is necessary considering the _surveyRepo on the next line will invoke SaveChanges() as well
            _questionRepo.Create(q);
            return true;
        }

        public bool ChangeQuestion(Guid questionId, string questionText, int sortId)
        {
            var question = _questionRepo.ReadById(questionId);
            if (question == null) return false;
            question.QuestionText = questionText;
            question.SortId = sortId;

            _questionRepo.Update(question);
            return true;
        }

        public bool DeleteQuestion(Guid questionId)
        {
            _questionRepo.Delete(questionId);
            return true;
        }
    }
}