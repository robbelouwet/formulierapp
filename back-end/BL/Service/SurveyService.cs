﻿using System;
using System.Collections;
using System.Collections.Generic;
using DAL;
using DAL.Repository.EF.Factory;
using DAL.Repository.Generic;
using Domain.Survey;
using Domain.Survey.Question;

namespace BL.Service
{
    /// <summary>
    /// This Service class exists for managing Questions individually, not the SurveyQuestions of a Survey sepcifically.
    /// <see cref="SurveyService"/> for managing Surveys and SurveyQuestions
    /// </summary>
    public class SurveyService : ISurveyService
    {
        private readonly ISurveyRepository _surveyRepo;

        public SurveyService(ISurveyRepository surveySurveyRepo)
        {
            _surveyRepo = surveySurveyRepo;

        }

        ////CHECKLISTS:
        /// 
        public Survey GetSurvey(Guid surveyId)
        {
            return _surveyRepo.ReadById(surveyId);
        }

        public IEnumerable<Survey> GetAllSurveys()
        {
            return _surveyRepo.ReadAll();
        }

        public Survey GetSurveyQuestionsAndAnswers(Guid surveyId)
        {
            return _surveyRepo.ReadQuestionsAndAnswers(surveyId);
        }

        public void RemoveSurvey(Guid surveyId)
        {
            _surveyRepo.Delete(surveyId);
        }
        
        // public Guid CreateSurveyTemplate()
        // {
        //     Survey list = new Survey();
        //     SurveyFactory.FillWithTemplateData(list, );
        //     _surveyRepo.Create(list);
        //     return list.Id;
        // }

        public void AnswerSurveyQuestion(Guid surveyId, Guid surveyQustionId, string answerText)
        {
            var survey = _surveyRepo.ReadQuestionsAndAnswers(surveyId);
            var surveyQuestion = survey.SurveyEntries.Find(sq => sq.Id == surveyQustionId);
            surveyQuestion.Answer = answerText;
            _surveyRepo.Update(survey);
        }
    }
}