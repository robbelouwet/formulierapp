﻿using System;

namespace Domain.Survey
{
    public class QuestionCategory
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? SortId { get; set; }

        public QuestionCategory(string name, string description, int sortId)
        {
            Name = name;
            Description = description;
            SortId = sortId;
        }

        public QuestionCategory()
        {
            
        }
    }
}