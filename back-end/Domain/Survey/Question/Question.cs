﻿using System;

namespace Domain.Survey.Question
{
    public class Question
    {
        public Guid Id { get; set; }

        //Om questions te kunnen sorteren in front-end
        public int? SortId { get; set; }
        
        // Welke questions gaan we groeperen in de front end?
        public QuestionCategory Category { get; set; }
        
        public string QuestionText { get; set; }
    }
}