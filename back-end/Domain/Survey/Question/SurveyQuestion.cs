﻿using System;

namespace Domain.Survey.Question
{
    public class SurveyQuestion
    {
        public Guid Id { get; set; }
        
        public Question Question { get; set; }
        public string Answer { get; set; }
    }
}
