﻿using System;
using System.Collections.Generic;
using Domain.Survey.Question;
using Domain.User;

namespace Domain.Survey
{
    public class Survey
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<ApplicationUser> Stakeholders { get; set; }
        public List<SurveyQuestion> SurveyEntries { get; set; }

        public Survey()
        {
            SurveyEntries = new List<SurveyQuestion>();
        }
    }
}