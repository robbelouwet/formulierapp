﻿using System;
using Microsoft.AspNetCore.Identity;

namespace Domain.User
{
    public class ApplicationUser : IdentityUser<Guid> 
    {
        public string Description { get; set; }
    }
}