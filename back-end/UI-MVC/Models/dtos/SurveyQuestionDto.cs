﻿using System.ComponentModel.DataAnnotations;

namespace FormulierApp.Models.dtos
{
    public class SurveyQuestionDto
    {
        [Required]
        public string QuestionId { get; set; }
        [Required]
        public string Answer { get; set; }
    }
}