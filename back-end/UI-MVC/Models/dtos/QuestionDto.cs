﻿using Domain.Survey;

namespace FormulierApp.Models.dtos
{
    public class QuestionDto
    {
        public string QuestionText { get; set; }
        public QuestionCategory Category { get; set; }
        public int SortId { get; set; }
    }
}