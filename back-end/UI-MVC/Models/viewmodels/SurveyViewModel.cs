﻿using System.Collections.Generic;
using FormulierApp.Models.dtos;

namespace FormulierApp.Models.viewmodels
{
    public class SurveyViewModel
    {
        public IEnumerable<SurveyQuestionDto> AnswerDtos { get; set; }
    }
}