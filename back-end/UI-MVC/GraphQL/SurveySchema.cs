﻿using FormulierApp.GraphQL.Types;
using GraphQL;
using GraphQL.Types;

namespace FormulierApp.GraphQL
{
    public class SurveySchema: Schema
    {
        public SurveySchema(IDependencyResolver dependencyResolver) : base(dependencyResolver)
        {
            //Elke property in deze class moet een interface returnen
            //M.a.w ge kunt ni ni rechtstreeks Modals vanuit Domain returnen
            Query = dependencyResolver.Resolve<SurveyQuery>();
            //Mutation
            Mutation = dependencyResolver.Resolve<SurveyMutation>();
            //Subscription
        }
    }
}