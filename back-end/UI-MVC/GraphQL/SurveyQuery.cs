﻿using System;
using DAL.Repository.Generic;
using FormulierApp.GraphQL.Types.QueryTypes.Survey;
using GraphQL.Types;

namespace FormulierApp.GraphQL
{
    public class SurveyQuery : ObjectGraphType
    {
        public SurveyQuery(ISurveyRepository surveyRepository, IQuestionCategoryRepository questionCategoryRepository)
        {
            // Dit is een field van een graphQL list van GraphQL types van Questions
            // Deze query zal deze lijst dus kunnen queryen omdat de resolve specifieert hoe
            Field<ListGraphType<SurveyType>>(
                "allSurveys",
                resolve: ctx => surveyRepository.ReadAll()
            );

            Field<SurveyType>(
                "surveyWithQuestions",
                arguments: new QueryArguments(new QueryArgument<NonNullGraphType<IdGraphType>> {Name = "surveyId"}),
                resolve: ctx => surveyRepository.ReadById(ctx.GetArgument<Guid>("surveyId"))
            );

            Field<ListGraphType<QuestionCategoryType>>(
                "allQuestionCategories",
                resolve: ctx => questionCategoryRepository.ReadAll()
            );
        }
    }
}