﻿using DAL.Repository.Generic;
using Domain.Survey;
using FormulierApp.GraphQL.Types.MutationTypes;
using FormulierApp.GraphQL.Types.QueryTypes.Survey;
using GraphQL.Types;

namespace FormulierApp.GraphQL
{
    public class SurveyMutation : ObjectGraphType
    {
        public SurveyMutation(ISurveyRepository surveyRepository)
        {
            Field<SurveyType>(
                "updateSurvey",
                arguments: new QueryArguments(new QueryArgument<NonNullGraphType<SurveyInputType>> {Name = "survey"}),
                resolve: ctx =>
                {
                    var iets = surveyRepository.Update(ctx.GetArgument<Survey>("survey"));
                    return iets;
                });
            
            Field<SurveyType>(
                "createSurvey",
                arguments: new QueryArguments(new QueryArgument<NonNullGraphType<SurveyInputType>> {Name = "survey"}),
                resolve: ctx =>
                {
                    var iets = surveyRepository.Create(ctx.GetArgument<Survey>("survey"));
                    return iets;
                });
        }
    }
}