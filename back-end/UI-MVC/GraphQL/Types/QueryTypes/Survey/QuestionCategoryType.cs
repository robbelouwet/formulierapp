﻿using Domain.Survey;
using GraphQL.Types;

namespace FormulierApp.GraphQL.Types.QueryTypes.Survey
{
    public class QuestionCategoryType : ObjectGraphType<QuestionCategory>
    {
        public QuestionCategoryType()
        {
            Field(qc => qc.Id);
            Field(qc => qc.Name);
            Field(qc => qc.Description);
            Field(qc => qc.SortId, type: typeof(IntGraphType));
        }
    }
}