﻿using DAL.Repository.Generic;
using FormulierApp.GraphQL.Types.QueryTypes.Survey.Question;
using GraphQL.Types;

namespace FormulierApp.GraphQL.Types.QueryTypes.Survey
{
    public class SurveyType : ObjectGraphType<Domain.Survey.Survey>
    {
        public SurveyType(ISurveyRepository surveyRepository)
        {
            Field(s => s.Id);
            Field(s => s.Name);
            Field(s => s.Description);
            
            Field<ListGraphType<ApplicationUserType>>("stakeholders",
                resolve: ctx => surveyRepository.ReadWithStakeholders(ctx.Source.Id).Stakeholders);
            
            Field<ListGraphType<SurveyQuestionType>>("surveyEntries",
                resolve: ctx => surveyRepository.ReadQuestionsAndAnswers(ctx.Source.Id).SurveyEntries);
        }
    }
}