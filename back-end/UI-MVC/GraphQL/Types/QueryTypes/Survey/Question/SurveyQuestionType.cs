﻿using Domain.Survey.Question;
using GraphQL.Types;

namespace FormulierApp.GraphQL.Types.QueryTypes.Survey.Question
{
    public class SurveyQuestionType : ObjectGraphType<SurveyQuestion>
    {
        public SurveyQuestionType()
        {
            Field(sq => sq.Id);
            Field(sq => sq.Answer);
            Field(sq => sq.Question, type: typeof(QuestionType));
        }
    }
}