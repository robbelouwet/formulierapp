﻿using GraphQL.Types;

namespace FormulierApp.GraphQL.Types.QueryTypes.Survey.Question
{
    public class QuestionType : ObjectGraphType<Domain.Survey.Question.Question>
    {
        public QuestionType()
        {
            Field(q => q.Id);
            Field(q => q.QuestionText);
            Field(q => q.SortId, type: typeof(IntGraphType));
            Field<QuestionCategoryType>("category", "The category of the question");
        }
    }
}