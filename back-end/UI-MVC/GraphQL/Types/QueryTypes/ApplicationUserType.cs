﻿using Domain.User;
using GraphQL.Types;

namespace FormulierApp.GraphQL.Types.QueryTypes
{
    public class ApplicationUserType : ObjectGraphType<ApplicationUser>
    {
        public ApplicationUserType()
        {
            Field(usr => usr.UserName, type: typeof(StringGraphType));
            Field(usr => usr.Description, type: typeof(StringGraphType));
            Field(usr => usr.Email, type: typeof(StringGraphType));
        }
    }
}