﻿using GraphQL.Types;

namespace FormulierApp.GraphQL.Types.MutationTypes
{
    public class InputQuestionCategoryType : InputObjectGraphType
    {
        public InputQuestionCategoryType()
        {
            Field<IdGraphType>("id");
            Field<NonNullGraphType<StringGraphType>>("name");
            Field<StringGraphType>("description");
            Field<StringGraphType>("sortId");
        }
    }
}