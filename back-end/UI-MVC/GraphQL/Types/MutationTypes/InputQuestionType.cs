﻿using GraphQL.Types;

namespace FormulierApp.GraphQL.Types.MutationTypes
{
    public class InputQuestionType : InputObjectGraphType
    {
        public InputQuestionType()
        {
            Field<IdGraphType>("id");
            Field<IntGraphType>("sortId");
            Field<StringGraphType>("questionText");
            Field<NonNullGraphType<InputQuestionCategoryType>>("category");
        }
    }
}