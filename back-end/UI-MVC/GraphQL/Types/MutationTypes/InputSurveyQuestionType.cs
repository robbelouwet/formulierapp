﻿using GraphQL.Types;

namespace FormulierApp.GraphQL.Types.MutationTypes
{
    public class InputSurveyQuestionType : InputObjectGraphType
    {
        public InputSurveyQuestionType()
        {
            Field<IdGraphType>("id");
            Field<StringGraphType>("answer");
            Field<NonNullGraphType<InputQuestionType>>("question");
        }
    }
}