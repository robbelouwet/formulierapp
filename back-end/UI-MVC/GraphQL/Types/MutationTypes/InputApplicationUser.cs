﻿using Domain.User;
using GraphQL.Types;

namespace FormulierApp.GraphQL.Types.MutationTypes
{
    public class InputApplicationUser : InputObjectGraphType
    {
        public InputApplicationUser()
        {
            Field<IdGraphType>("id");
            Field<StringGraphType>("email");
            Field<StringGraphType>("name");
        }
    }
}