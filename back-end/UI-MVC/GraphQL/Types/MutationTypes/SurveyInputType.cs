﻿using Domain.Survey;
using GraphQL.Types;

namespace FormulierApp.GraphQL.Types.MutationTypes
{
    public class SurveyInputType : InputObjectGraphType<Survey>
    {
        public SurveyInputType()
        {
            Field<IdGraphType>("id");
            Field<StringGraphType>("name");
            Field<StringGraphType>("description");
            Field<ListGraphType<InputApplicationUser>>("stakeholders");
            Field<ListGraphType<InputSurveyQuestionType>>("surveyEntries");
        }
    }
}