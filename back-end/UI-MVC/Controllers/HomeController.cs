﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using BL;
using BL.Service;
using Domain.Survey;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using FormulierApp.Models;

namespace FormulierApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ISurveyService _surveyService;

        public HomeController(ILogger<HomeController> logger, ISurveyService surveyService)
        {
            _logger = logger;
            _surveyService = surveyService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var template = _surveyService.GetSurveyQuestionsAndAnswers(new Guid("b40fabf6-2b44-461b-a2ce-b11c6f867acb"));
            
            // Guid id = _surveyService.CreateSurveyTemplate();
            // var template = _surveyService.GetSurvey(id);
            return View(template);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
    }
}