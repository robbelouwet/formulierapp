﻿using System;
using BL;
using BL.Service;
using Domain.Survey;
using FormulierApp.Models.dtos;
using FormulierApp.Models.viewmodels;
using Microsoft.AspNetCore.Mvc;

namespace FormulierApp.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class SurveyController : ControllerBase
    {
        private readonly ISurveyService _surveyService;

        public SurveyController(ISurveyService surveyService)
        {
            _surveyService = surveyService;
        }


        [HttpGet("{surveyId}/Answers")]
        public IActionResult GetSurveyWithAnswers(Guid surveyId)
        {
            var survey = _surveyService.GetSurveyQuestionsAndAnswers(surveyId);
            if (survey == null) return NotFound();
            return Ok(survey);
        }

        [HttpGet("{surveyId}/Info")]
        public IActionResult GetSurveyWithInfo(Guid surveyId)
        {
            var survey = _surveyService.GetSurvey(surveyId);
            if (survey == null) return NotFound();
            return Ok(survey);
        }

        [HttpGet("All")]
        public IActionResult GetAllSurveys()
        {
            var surveys = _surveyService.GetAllSurveys();
            if (surveys == null) return NotFound();
            
            foreach (var survey in surveys)
            {
                _surveyService.GetSurveyQuestionsAndAnswers(survey.Id);
            }

            return Ok(surveys);
        }


        [HttpPost("Survey")]
        public IActionResult CreateSurvey(SurveyViewModel vm)
        {
            return Ok();
        }

        [HttpPut("Survey")]
        public IActionResult ChangeSurvey(SurveyViewModel vm)
        {
            //change the questions only
            return Ok();
        }

        [HttpPost("Answer")]
        public IActionResult AnswerSurveyQuestion(SurveyQuestionDto dto)
        {
            return Ok(); //als er geen exception gethrowed is, is alles gelukt
        }


        [HttpDelete("Survey")]
        public IActionResult RemoveSurvey(Guid listId)
        {
            _surveyService.RemoveSurvey(listId);
            return Ok();
        }
    }
}