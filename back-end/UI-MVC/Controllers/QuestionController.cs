﻿using System;
using BL;
using BL.Service;
using Domain.Survey.Question;
using FormulierApp.Models.dtos;
using Microsoft.AspNetCore.Mvc;

namespace FormulierApp.Controllers
{
    [Route("/api/[controller]")]
    public class QuestionController : Controller
    {
        private readonly IQuestionService _questionService;

        public QuestionController(IQuestionService questionService)
        {
            _questionService = questionService;
        }
        
        [HttpPut("Question")]
        public IActionResult UpdateQuestion(Guid questionId, string questionText, int sortId)
        {
            var q = _questionService.ChangeQuestion(questionId, questionText, sortId);
            if (q)
                return Ok();
            return BadRequest();
        }

        [HttpGet("Question")]
        public IActionResult GetQuestion(Guid questionId)
        {
            var q = _questionService.GetQuestion(questionId);
            if (q == null) return NotFound(q);
            return Ok(q);
        }

        [HttpPost("Question")]
        public IActionResult CreateQuestion(QuestionDto dto)
        {
            if (_questionService.CreateQuestion(dto.QuestionText, dto.SortId, dto.Category))
                return Ok();
            return BadRequest();
        }

        [HttpDelete("Question")]
        public IActionResult RemoveQuestion(Guid itemId)
        {
            if (_questionService.DeleteQuestion(itemId))
                return Ok();
            return BadRequest();
        }
    }
}