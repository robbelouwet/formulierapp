﻿﻿// JS Dependencies: Bootstrap & JQuery
import 'bootstrap';
import '../../node_modules/@fortawesome/fontawesome-free/css/all.min.css'
import '../css/custom.css'

// Using the next two lines is like including partial view _ValidationScriptsPartial.cshtml
import 'jquery-validation';
import 'jquery-validation-unobtrusive';

// CSS Dependencies: Bootstrap
import '../css/bootstrap-editable.css'
import 'bootstrap/dist/css/bootstrap.css';

// Custom JS imports
import './bootstrap-editable'
import {send} from './network'

// Custom CSS imports
let method='POST';
let url='/api/Survey/Answer';
let elements = $('.question');

send(elements, method, url);

//dit werkt om de label van de <a> element te vinden:
//console.log(document.getElementsByTagName('label')[??].attributes.getNamedItem('for').value);
// $.fn.editable.defaults.ajaxOptions = {type: 'post', dataType: 'json'};

console.log('The \'site\' bundle has been loaded!');
