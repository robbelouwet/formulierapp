$.fn.editable.defaults.mode = 'popup';

export function send(element, method, url) {
    $(document).ready(function () {
        $('.question').editable({
            url: url,
            params: {
                surveyId: document.getElementById("surveyId").textContent
            },
            ajaxOptions: {
                type: method,
                contentType: 'application/x-www-form-urlencoded ',
                dataType: 'json'
            }
        });
    });
}
