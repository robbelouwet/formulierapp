﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using DAL.Repository.EF.Database;
using Domain.Survey;
using Domain.Survey.Question;

namespace DAL.Repository.EF.Factory
{
    public class SurveyFactory
    {
        private readonly SurveyDbContext _ctx;

        public SurveyFactory(SurveyDbContext ctx)
        {
            _ctx = ctx;
        }

        public static Dictionary<string, QuestionCategory> GetTemplateCategories(SurveyDbContext ctx)
        {
            var allCategories = new Dictionary<string, QuestionCategory>();

            var info = new QuestionCategory("Info", "Basic info about this Survey.", 1);
            var incident = new QuestionCategory("Incident", "Basic info about the incident itself.", 2);
            var aircraftMake = new QuestionCategory("Aircraft Make", "Info about the aircraft make.", 3);
            var pilotDocuments = new QuestionCategory("Pilot Documents", "Documents the pilot neededs to have", 4);
            var aircraftDocuments =
                new QuestionCategory("Aircraft Documents", "Documents required to have been on board", 5);
            var aircraftDetails =
                new QuestionCategory("Aircraft Details", "More detailed information about the aircraft.", 6);
            var occuranceDocuments = new QuestionCategory("Occurance Documents", "Documents to have on occurance.", 7);
            var extra = new QuestionCategory("Extra remarks",
                "Extra usefull remarks like info about third party claims.", 8);
            QuestionCategory[] categories =
            {
                info, incident, aircraftMake, pilotDocuments, aircraftDocuments, aircraftDetails, occuranceDocuments,
                extra
            };
            
            foreach (var c in categories)
            {
                var found = ctx.QuestionCategories.SingleOrDefault(qc => qc.Name == c.Name);
                if (found != null)
                {
                    allCategories.Add(found.Name, found);
                }
                else
                {
                    ctx.QuestionCategories.Add(c);
                    ctx.SaveChanges();
                    allCategories.Add(c.Name, c);
                }
            }

            return allCategories;
        }

        /*
        public static void FillWithTemplateData(Survey list)
        {

            //Een beetje inefficïent, maar soit
            SurveyQuestion[] lijstje =
            {
                new SurveyQuestion
                {
                    Question = new Question()
                    {
                        QuestionText = "Date",
                        Category = QuestionCategory.Info,
                        SortId = 1
                    }
                },
                new SurveyQuestion
                {
                    Question = new Question()
                    {
                        QuestionText = "Insurance Co",
                        Category = QuestionCategory.Info,
                        SortId = 2
                    }
                },
                new SurveyQuestion
                {
                    Question = new Question()
                    {
                        QuestionText = "Y.ref",
                        Category = QuestionCategory.Info,
                        SortId = 3
                    }
                },
                new SurveyQuestion
                {
                    Question = new Question()
                    {
                        QuestionText = "O.ref",
                        Category = QuestionCategory.Info,
                        SortId = 4
                    }
                },
                new SurveyQuestion
                {
                    Question = new Question()
                    {
                        QuestionText = "Date",
                        Category = QuestionCategory.Incident,
                        SortId = 1
                    }
                },
                new SurveyQuestion
                {
                    Question = new Question()
                    {
                        QuestionText = "Insured amount",
                        Category = QuestionCategory.Incident,
                        SortId = 2
                    }
                },
                new SurveyQuestion
                {
                    Question = new Question()
                    {
                        QuestionText = "Deductable",
                        Category = QuestionCategory.Incident,
                        SortId = 3
                    }
                },
                new SurveyQuestion
                {
                    Question = new Question()
                    {
                        QuestionText = "Aircraft Make",
                        Category = QuestionCategory.AircraftMake,
                        SortId = 1
                    }
                },
                new SurveyQuestion
                {
                    Question = new Question()
                    {
                        QuestionText = "Type",
                        Category = QuestionCategory.AircraftMake,
                        SortId = 2
                    }
                },
                new SurveyQuestion
                {
                    Question = new Question()
                    {
                        QuestionText = "Imm",
                        Category = QuestionCategory.AircraftMake,
                        SortId = 3
                    }
                },
                new SurveyQuestion
                {
                    Question = new Question()
                    {
                        QuestionText = "SN",
                        Category = QuestionCategory.AircraftMake,
                        SortId = 4
                    }
                },
                new SurveyQuestion
                {
                    Question = new Question()
                    {
                        QuestionText = "Owner/Issuer",
                        Category = QuestionCategory.AircraftMake,
                        SortId = 5
                    }
                },
                new SurveyQuestion
                {
                    Question = new Question()
                    {
                        QuestionText = "Tel",
                        Category = QuestionCategory.AircraftMake,
                        SortId = 6
                    }
                },
                new SurveyQuestion
                {
                    Question = new Question()
                    {
                        QuestionText = "Aircraft Make",
                        Category = QuestionCategory.AircraftMake,
                        SortId = 7
                    }
                },
                new SurveyQuestion
                {
                    Question = new Question()
                    {
                        QuestionText = "VAT DEDUCTABLE?",
                        Category = QuestionCategory.AircraftMake,
                        SortId = 8
                    }
                },
                new SurveyQuestion
                {
                    Question = new Question()
                    {
                        QuestionText = "Pilot",
                        Category = QuestionCategory.AircraftMake,
                        SortId = 9
                    }
                },
                new SurveyQuestion
                {
                    Question = new Question()
                    {
                        QuestionText = "Tel",
                        Category = QuestionCategory.AircraftMake,
                        SortId = 10
                    }
                },
                new SurveyQuestion
                {
                    Question = new Question()
                    {
                        QuestionText = "Email",
                        Category = QuestionCategory.AircraftMake,
                        SortId = 11
                    }
                },
                new SurveyQuestion
                {
                    Question = new Question()
                    {
                        QuestionText = "Workshop Contact",
                        Category = QuestionCategory.AircraftMake,
                        SortId = 12
                    }
                },
                new SurveyQuestion
                {
                    Question = new Question()
                    {
                        QuestionText = "Tel",
                        Category = QuestionCategory.AircraftMake,
                        SortId = 13
                    }
                },
                new SurveyQuestion
                {
                    Question = new Question()
                    {
                        QuestionText = "Email",
                        Category = QuestionCategory.AircraftMake,
                        SortId = 14
                    }
                },
                new SurveyQuestion
                {
                    Question = new Question()
                    {
                        QuestionText = "Other",
                        Category = QuestionCategory.AircraftMake,
                        SortId = 15
                    }
                }
            };
            list.SurveyEntries.AddRange(lijstje);
        }*/

        /// <summary>
        /// The puropse of this method is to allow dynamicaly loading navigational properties from a generic entity from database without creating a new method each time a different property needs to be loaded from the database
        /// </summary>
        /// <param name="obj">The object of which certain properties need to be loaded from the database.</param>
        /// <param name="expression">a lambda that specifies which properties need to be loaded from this entity</param>
        /// <param name="loadAll">true if every referenceEntry of the entity needs to be loaded at once from the database
        /// , false if only a reference needs to be made (best practice if multiple properties need to be loaded, only Load after every reference is made)</param>
        /// <typeparam name="T"></typeparam>
        public void ReferenceNavProperty<T>(T obj, Expression<Func<T, object>> expression, bool loadAll)
            where T : class
        {
            var reference = _ctx.Entry(obj).Reference(expression);
            if (reference != null)
                _ctx.Entry(obj).References.Append(reference);

            if (loadAll)
                LoadAllReferenced(obj);
        }

        /// <summary>
        /// Equivalent to ReferenceNavProperty, but specifically for IEnumerable properties.
        /// <see cref="ReferenceNavProperty{T}"/>
        /// </summary>
        public void ReferenceNavEnumProperty<T>(T obj, Expression<Func<T, IEnumerable<object>>> expression,
            bool loadAll)
            where T : class
        {
            var collection = _ctx.Entry(obj).Collection(expression);

            if (collection != null)
                _ctx.Entry(obj).Collections.Append(collection);


            if (loadAll)
                LoadAllReferenced(obj);
        }

        private void LoadAllReferenced<T>(T obj) where T : class
        {
            foreach (var reference in _ctx.Entry(obj).References)
            {
                reference.Load();
            }

            foreach (var collection in _ctx.Entry(obj).Collections)
            {
                collection.Load();
            }
        }
    }
}