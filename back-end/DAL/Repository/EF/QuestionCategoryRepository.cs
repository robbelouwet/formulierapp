﻿using System;
using DAL.Repository.EF.Database;
using DAL.Repository.Generic;
using Domain.Survey;

namespace DAL.Repository.EF
{
    public class QuestionCategoryRepository : Repository<Guid, QuestionCategory>, IQuestionCategoryRepository
    {
        public QuestionCategoryRepository(SurveyDbContext ctx) : base(ctx)
        {
        }
    }
}