﻿using System;
using System.Collections.Generic;
using System.Linq;
using DAL.Repository.EF.Database;
using DAL.Repository.Generic;

namespace DAL.Repository.EF
{
    public class Repository<TK, T> : IRepository<TK, T>
    where TK : struct
    where T : class
    {
        internal readonly SurveyDbContext _ctx;

        public Repository(SurveyDbContext ctx)
        {
            _ctx = ctx;
        }
        public IEnumerable<T> ReadAll()
        {
            return _ctx.Set<T>().AsEnumerable();
        }

        public IEnumerable<T> ReadFiltered(Predicate<T> p)
        {
            return _ctx.Set<T>().Where(p.Invoke);
        }

        public T ReadById(TK key)
        {
            return _ctx.Set<T>().Find(key);
        }

        public T Create(T obj)
        {
            _ctx.Add(obj);
            _ctx.SaveChanges();
            return obj;
        }

        public T Update(T obj)
        {
            _ctx.Update(obj);
            _ctx.SaveChanges();
            return obj;
        }

        public bool Delete(TK key)
        {
            var obj = _ctx.Set<T>().Find(key);
            if (obj != null)
            {
                _ctx.Remove(obj);
                _ctx.SaveChanges();
                return true;
            }
            return false;
        }
    }
}