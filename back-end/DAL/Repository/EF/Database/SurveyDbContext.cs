﻿using System;
using Domain.Survey;
using Domain.Survey.Question;
using Domain.User;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repository.EF.Database
{
    public class SurveyDbContext : IdentityDbContext<ApplicationUser, IdentityRole<Guid>, Guid>
    {
        public DbSet<Survey> Surveys { get; set; }
        public DbSet<SurveyQuestion> SurveyQuestions { get; set; }
        public DbSet<Question> Questions { get; set; }

        public DbSet<QuestionCategory> QuestionCategories { get; set; }
        // public DbSet<Answer> Answers { get; set; }

        public SurveyDbContext()
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=SurveyDB.db");
            optionsBuilder.EnableSensitiveDataLogging();
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Question>().Property<Guid>("FK_QuestionCategory");
            builder.Entity<Question>()
                .HasOne(q => q.Category)
                .WithMany()
                .HasForeignKey("FK_QuestionCategory")
                .OnDelete(DeleteBehavior.SetNull);

            #region PK's
            builder.Entity<Survey>().HasKey(cl => cl.Id);
            builder.Entity<Question>().HasKey(q => q.Id);
            
            //composite key would be even better (pk question + pk survey)
            builder.Entity<SurveyQuestion>().HasKey(sq => sq.Id);
            #endregion
            
            #region basic relaties
            // A SurveyQuestion has exactly 1 Question
            // Whenever a Question is deleted, all linked SurveyQuestions are deleted as well
            // (when those SurveyQuestions are deleted, the linked Answers are deleted as well)
            builder.Entity<SurveyQuestion>()
                .HasOne(sq=>sq.Question)
                .WithMany()
                .IsRequired(); // IsRequired -> OnDelete cascade -> if a question is deleted, all SurveyQuestion with that question get deleted

            // A Survey has 0 or more SurveyQuestions
            // When a Survey is deleted, all SurveyQuestions are deleted as well
            // (answers linked to thos SurveyQuestions are deleted as well, questions exist on multiple surveys and are not deleted)
            builder.Entity<SurveyQuestion>()
                .HasOne<Survey>()
                .WithMany(s => s.SurveyEntries)
                .IsRequired();
            #endregion

        }
    }
}