﻿using System;
using DAL.Repository.EF.Factory;
using Domain.Survey;
using Domain.Survey.Question;
using Microsoft.Extensions.DependencyInjection;

namespace DAL.Repository.EF.Database
{
    public static class SurveyDbInitializer
    {
        private static SurveyDbContext _ctx;
        private static bool _isSeeded;

        public static void Initialize(IServiceProvider serviceScope)
        {
            _ctx = serviceScope.GetRequiredService<SurveyDbContext>();

            _ctx.Database.EnsureDeleted();
            _ctx.Database.EnsureCreated();
            if (!_isSeeded)
            {
                _ctx.Database.EnsureDeleted();
                _ctx.Database.EnsureCreated();
                Seed();
                _isSeeded = true;
            }
        }

        private static void Seed()
        {
            var categories = SurveyFactory.GetTemplateCategories(_ctx);

            
            Survey survey = new Survey()
            {
                Name = "Naam van de survey",
                Description = "Dit is de omschrijving van de test survey.",
                Id = new Guid("b40fabf6-2b44-461b-a2ce-b11c6f867acb")
            };

            var naam = new SurveyQuestion()
            {
                Id = new Guid("e4a71787-2915-4256-92ec-185005ee5a33"),
                Question = new Question
                {
                    QuestionText = "Naam1",
                    SortId = 1,
                    Category = categories["Aircraft Details"]
                },
                Answer = "Robbe"
            };
            var naam2 = new SurveyQuestion()
            {
                Id = new Guid("e4a71787-2915-4256-92ec-185005ee5a34"),
                Question = new Question
                {
                    QuestionText = "Naam2",
                    SortId = 2,
                    Category = categories["Aircraft Details"]
                },
                Answer = "Robbe"
            };
            var naam3 = new SurveyQuestion()
            {
                Id = new Guid("e4a71787-2915-4256-92ec-185005ee5a35"),
                Question = new Question
                {
                    QuestionText = "Naam3",
                    SortId = 3,
                    Category = categories["Aircraft Details"]
                },
                Answer = "Robbe"
            };
            var naam4 = new SurveyQuestion()
            {
                Id = new Guid("e4a71787-2915-4256-92ec-185005ee5a36"),
                Question = new Question
                {
                    QuestionText = "Naam4",
                    SortId = 4,
                    Category = categories["Aircraft Details"]
                },
                Answer = "Robbe"
            };

            var achternaam = new SurveyQuestion()
            {
                Id = new Guid("55389e50-52da-4a2f-8664-4df61be2ff45"),
                Question = new Question
                {
                    QuestionText = "Achternaam",
                    SortId = 2,
                    Category = categories["Occurance Documents"]
                },
                Answer = "Louwet"
            };

            survey.SurveyEntries.Add(naam);
            survey.SurveyEntries.AddRange(new []{achternaam, naam2, naam3, naam4});

            _ctx.SurveyQuestions.AddRange(naam, naam2, naam3, naam4, achternaam);
            _ctx.Surveys.Add(survey);
            _ctx.SaveChanges();
        }
    }
}