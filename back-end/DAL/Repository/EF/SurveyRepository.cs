﻿using System;
using System.Collections.Generic;
using System.Linq;
using DAL.Repository.EF.Database;
using DAL.Repository.Generic;
using Domain.Survey;
using Domain.User;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repository.EF
{
    public class SurveyRepository : Repository<Guid, Survey>, ISurveyRepository
    {
        public SurveyRepository(SurveyDbContext ctx) : base(ctx)
        {
        }

        public Survey ReadQuestionsAndAnswers(Guid surveyId)
        {
            var survey = ReadById(surveyId);
            _ctx.Surveys
                .Include(s => s.SurveyEntries)
                .ThenInclude(e => e.Question)
                .ThenInclude(q=>q.Category)

                /*.Include(s => s.SurveyEntries)
                .ThenInclude(e => e.Answer)*/.Load();

            return survey;
        }

        public Survey ReadWithStakeholders(Guid surveyId)
        {
            return _ctx.Surveys.Include(s => s.Stakeholders).Single(s => s.Id == surveyId);
        }
    }
}