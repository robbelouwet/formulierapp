﻿using System;
using System.Collections.Generic;
using System.Linq;
using DAL.Repository.EF.Database;
using DAL.Repository.Generic;
using Domain.Survey.Question;

namespace DAL.Repository.EF
{
    public class QuestionRepository : Repository<Guid, Question>, IQuestionRepository
    {
        public QuestionRepository(SurveyDbContext ctx) : base(ctx)
        {
        }
    }
}