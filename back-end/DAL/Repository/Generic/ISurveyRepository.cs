﻿using System;
using System.Collections.Generic;
using Domain.Survey;
using Domain.User;

namespace DAL.Repository.Generic
{
    /// <summary>
    /// functionality that uses hard coupling to <see cref="Survey"/> belong heres
    /// </summary>
    public interface ISurveyRepository : IRepository<Guid, Survey>
    {
        Survey ReadQuestionsAndAnswers(Guid surveyId);
        Survey ReadWithStakeholders(Guid surveyId);
    }
}