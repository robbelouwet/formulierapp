﻿using System;
using Domain.Survey.Question;

namespace DAL.Repository.Generic
{
    public interface IQuestionRepository : IRepository<Guid, Question>
    {
    }
}