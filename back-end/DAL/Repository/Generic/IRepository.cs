﻿using System;
using System.Collections.Generic;

namespace DAL.Repository.Generic
{
    public interface IRepository<TK, T>
    {
        IEnumerable<T> ReadAll();
        IEnumerable<T> ReadFiltered(Predicate<T> p);
        T ReadById(TK id);
        T Create(T obj);
        T Update(T obj);
        bool Delete(TK id);
    }
}