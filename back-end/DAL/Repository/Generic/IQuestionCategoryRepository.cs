﻿using System;
using Domain.Survey;

namespace DAL.Repository.Generic
{
    public interface IQuestionCategoryRepository : IRepository<Guid, QuestionCategory>
    {
    }
}